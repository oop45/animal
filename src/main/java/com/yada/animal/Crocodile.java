/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.animal;

/**
 *
 * @author ASUS
 */
public class Crocodile extends Reptile {
    private String speciesName;

    public Crocodile(String speciesName) {
        super("Crocodile", 4);
        this.speciesName = speciesName;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: " + speciesName + " belly crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: " + speciesName + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: " + speciesName + " high walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: " + speciesName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: " + speciesName + " sleep");
    }
    
}
