/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.animal;

/**
 *
 * @author ASUS
 */
public class Bat extends Poultry{
    private String speciesName;

    public Bat(String speciesName) {
        super("Bat", 4);
        this.speciesName = speciesName;
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + speciesName + " fly");
    }

    @Override
    public void eat() {
       System.out.println("Bat: " + speciesName + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + speciesName + " walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + speciesName + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + speciesName + " sleep");
    }
    
}
