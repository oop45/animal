/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.animal;

/**
 *
 * @author ASUS
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Dang");
        h1.eat();
        h1.walk();
        h1.run();
        System.out.println("h1 is animal ? " + (h1 instanceof Animal));
        System.out.println("h1 is land animal ? " + (h1 instanceof LandAnimal));
        
        Animal a1 = h1;
        System.out.println("a1 is land animal ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is reptile animal ? " + (a1 instanceof Reptile));
        
        Cat cat1 = new Cat("Bumpkin");
        cat1.eat();
        cat1.speak();
        cat1.run();
        cat1.sleep();
        Animal a2 = cat1;
        System.out.println("a2 is land animal ? " + (a2 instanceof LandAnimal));
        System.out.println("a2 is Aquatic animal ? " + (a2 instanceof Reptile));
        
        Dog dog1 = new Dog("Zero");
        dog1.sleep();
        dog1.speak();
        dog1.walk();
        dog1.run();
        System.out.println("d1 is land animal ? " + (dog1 instanceof LandAnimal));
        Animal a3 = dog1;
        System.out.println("a3 is poultry ? " + (a3 instanceof Poultry));
        
        Snake s1 = new Snake("Cobra");
        s1.crawl();
        s1.sleep();
        s1.walk();
        s1.eat();
        System.out.println("s1 is animal ? " + (s1 instanceof Animal));
        Animal a4 = s1;
        System.out.println("a4 is aquatic animal ? " + (a4 instanceof AquaticAnimal));
        
        Crocodile ccd1 = new Crocodile("Nile");
        ccd1.crawl();
        ccd1.walk();
        ccd1.eat();
        System.out.println("ccd1 is reptile ? " + (ccd1 instanceof Reptile));
        Animal a5 = ccd1;
        System.out.println("a5 is land animal ? " + (a5 instanceof LandAnimal));
        
        Fish f1 = new Fish("Gold");
        f1.eat();
        f1.swim();
        f1.sleep();
         System.out.println("f1 is animal ? " + (f1 instanceof Animal));
        Animal a6 = f1;
        System.out.println("a6 is poultry ? " + (a6 instanceof Poultry));
        
        Crab crab1 = new Crab("Poo");
        crab1.eat();
        crab1.sleep();
        crab1.swim();
        crab1.walk();
         System.out.println("crab1 is aquatic animal ? " + (crab1 instanceof AquaticAnimal));
        Animal a7 = crab1;
        System.out.println("a7 is land animal ? " + (a6 instanceof LandAnimal));
        
        Bat b1 = new Bat("Hoary");
        b1.eat();
        b1.fly();
        b1.sleep();
         System.out.println("b1 is animal ? " + (b1 instanceof Animal));
        Animal a8 = b1;
        System.out.println("a8 is reptile ? " + (a8 instanceof Reptile));
        
        Bird bi1 = new Bird("Tweety");
        bi1.eat();
        bi1.speak();
        bi1.fly();
        System.out.println("bi1 is animal ? " + (bi1 instanceof Poultry));
        Animal a9 = bi1;
        System.out.println("a9 is land animal ? " + (a4 instanceof LandAnimal));
        
        System.out.println("------------------------------------");
        
        Plane plane = new Plane("Engine No.1");
        Car car = new Car("Engine No.0");
        
        Flyable[] flyables = {b1,bi1,plane};
        for (Flyable f : flyables){
            if (f instanceof Plane) {
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runables = {cat1,dog1,car};
        for (Runable r : runables) {
            if (r instanceof Car) {
                Car c = (Car)r;
                c.startEngine();
            }
            r.run();
        }
        
        Swimable[] swimables = {crab1,f1};
        for (Swimable s : swimables) {
            s.swim();
        }
        
        Crawlable[] crawlables = {ccd1,s1};
        for (Crawlable c : crawlables) {
            c.crawl();
        }
    }
    
}
